//
//  Tajawal_assessment_taskTests.swift
//  Tajawal assessment taskTests
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import XCTest
@testable import Tajawal_assessment_task

class Tajawal_assessment_taskTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRequestHotels() {
        let homeVC = HomeViewController()
        homeVC.requestHotelsAPI()
        assert(homeVC.hotelManager.delegate != nil)
    }
    
    func testGetHotelsAPI() {
        let url = URL.init(string: "https://api.myjson.com/bins/o3pbx")
        let session = URLSession.shared
        let expectations = expectation(description: "GET \(url)")
        let dataTask = session.dataTask(with: url!) {(data, response, error) in
            let res = response as! HTTPURLResponse
            expectations.fulfill()
            assert(res.statusCode == 200)
        }
        dataTask.resume()
        waitForExpectations(timeout: 40) { (error) in
            if error != nil {
                print(error!)
            }
            dataTask.cancel()
        }
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}

//
//  UIImageView+Download.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadImageFrom(_ url:String) {
        URLSession.shared.dataTask( with: URL.init(string: url)!, completionHandler: {
            (data, response, error) -> Void in
            if error == nil {
                DispatchQueue.main.async() {
                    self.contentMode =  .scaleAspectFill
                    if let data = data { self.image = UIImage(data: data) }
                }
            }
        }).resume()
    }
}

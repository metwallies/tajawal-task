//
//  HotelTableViewCell.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import UIKit

class HotelTableViewCell: UITableViewCell {

    @IBOutlet weak var hotelImageView: UIImageView!
    @IBOutlet weak var hotelName: UILabel!
    @IBOutlet weak var hotelAddress: UILabel!
    @IBOutlet weak var hotelHighRate: UILabel!
    @IBOutlet weak var hotelLowRate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCellWith(_ hotel : Hotel) {
        self.hotelImageView.image = nil
        self.hotelName.text = hotel.hotelName
        self.hotelAddress.text = hotel.address
        self.hotelHighRate.text = "AED \(hotel.highRate)"
        self.hotelLowRate.text = "AED \(hotel.lowRate)"
        if hotel.lowRate < hotel.highRate {
            let attributedString = NSMutableAttributedString(string: self.hotelHighRate.text!, attributes: [NSAttributedStringKey.strikethroughStyle: 1])
            self.hotelHighRate.attributedText = attributedString
        }
        self.hotelImageView.downloadImageFrom(hotel.image)
    }
}

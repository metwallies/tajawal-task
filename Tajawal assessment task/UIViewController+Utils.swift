//
//  UIViewController+Utils.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/10/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showError(_ error : String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let cancelAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAlertAction)
        self.showDetailViewController(alert, sender: self)
    }
    
    func showLoadingIndicator() {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        let bgView = UIView(frame: self.view.frame)
        activityIndicator.center = self.view.center
        bgView.addSubview(activityIndicator)
        bgView.tag = 1110
        bgView.backgroundColor = UIColor(displayP3Red: 0.4, green: 0.4, blue: 0.4, alpha: 0.5)
        activityIndicator.startAnimating()
        self.view.addSubview(bgView)
    }
    
    
    func hideLoadingIndicator() {
        
        for subView in self.view.subviews {
            if subView.tag == 1110 {
                subView.removeFromSuperview()
                break
            }
        }
    }
}

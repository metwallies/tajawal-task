//
//  HotelManager.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import UIKit

protocol HotelManagerDelegate : AnyObject {
    func getHotelDidSucceedWith(_ hotels : [Hotel])
    func getHotelDidFailWith(_ error : String)
}

class HotelManager: NSObject {

    weak var delegate : HotelManagerDelegate?
    
    func getHotels() {
        let url = URL.init(string: "https://api.myjson.com/bins/o3pbx")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url!) {[unowned self] (data, response, error) in
            if self.delegate != nil {
                if error == nil {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String : Any]
                        var hotelsArray = [Hotel]()
                        for dict in json["hotel"] as! [[String : Any]] {
                            let tempHotel = Hotel.init(withJson: dict)
                            if tempHotel.hotelId != 0 {
                                hotelsArray.append(tempHotel)
                            }
                        }
                        DispatchQueue.main.async {
                            self.delegate?.getHotelDidSucceedWith(hotelsArray)
                        }
                    }
                    catch {
                    }
                }
                else {
                    self.delegate?.getHotelDidFailWith((error?.localizedDescription)!)
                }
            }
        }
        dataTask.resume()
    }
}

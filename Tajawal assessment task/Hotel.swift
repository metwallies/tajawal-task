//
//  Hotel.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import UIKit
import MapKit
class Hotel: NSObject {
/*
     "hotelId":4020979,
     "image":[
     {
     "url":"https://az712897.vo.msecnd.net/images/full/A1EE945E-166C-4AC0-BB73-00B1D8F5DEF0.jpeg"
     }
     ],
     "location":{
     "address":"Burj Nahar Roundabout, Naif Road,",
     "latitude":25.275914,
     "longitude":55.313262
     },
     "summary":{
     "highRate":6386.04,
     "hotelName":"Coral Oriental Dubai",
     "lowRate":4958.58
     }
 */
    
    var hotelId = 0
    var image = ""
    var address = ""
    var hotelName = ""
    var lat : Double = 0.0
    var lon : Double = 0.0
    var highRate : Double = 0.0
    var lowRate : Double = 0.0
    
    override init() {
        super.init()
    }
    
    init(withJson json : [String : Any]) {
        super.init()
        hotelId = json["hotelId"] as! Int
        
        let imageArray = json["image"] as! [[String : String]]
        image = imageArray[0]["url"]!
        
        let location = json["location"] as! [String: Any]
        address     = location["address"] as! String
        lat         = location["latitude"] as! Double
        lon         = location["longitude"] as! Double
        
        let summary = json["summary"] as! [String : Any]
        hotelName   = summary["hotelName"] as! String
        highRate    = summary["highRate"] as! Double
        lowRate     = summary["lowRate"] as! Double
    }
    
    func setMapAnnotationPoint() -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.coordinate.latitude = self.lat
        annotation.coordinate.longitude = self.lon
        annotation.title = self.hotelName
        annotation.subtitle = self.address
        return annotation
    }
}

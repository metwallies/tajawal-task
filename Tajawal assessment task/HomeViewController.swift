//
//  ViewController.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: UIViewController, HotelManagerDelegate {

    //MARK: IBOUTLETS
    @IBOutlet weak var hotelsSearchBar: UISearchBar!
    @IBOutlet weak var hotelsTableView: UITableView!
    @IBOutlet weak var hotelsMapView: MKMapView!
    
    //MARK: VARIABLES
    var hotelsArray = [Hotel]()
    var hotelManager = HotelManager()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        
        return refreshControl
    }()
    var searchString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)

        
        self.hotelsTableView.addSubview(self.refreshControl)
        self.hotelsTableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func requestHotelsAPI() {
        self.showLoadingIndicator()
        hotelManager.delegate = self
        hotelManager.getHotels()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.requestHotelsAPI()
    }
    
    @objc func didBecomeActive() {
        self.requestHotelsAPI()
    }
    
    //MARK: HotelManagerDelegate
    func getHotelDidSucceedWith(_ hotels: [Hotel]) {
        hotelManager.delegate = nil
        self.hideLoadingIndicator()
        hotelsArray = hotels
        self.hotelsTableView.reloadData()
        refreshControl.endRefreshing()
        
        for hotel in hotels {
            let annotation = hotel.setMapAnnotationPoint()
            self.hotelsMapView.addAnnotation(annotation)
        }
        if hotels.count > 0 {
        let hotel = hotels[0]
            let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D.init(latitude: hotel.lat, longitude: hotel.lon), fromDistance: 4000.0, pitch: 0, heading: 0)
            self.hotelsMapView.setCamera(camera, animated: true)
        }
    }
    
    func getHotelDidFailWith(_ error: String) {
        hotelManager.delegate = nil
        self.showError(error)
        self.hideLoadingIndicator()
    }
}

//MARK: TABLEVIEW DELEGATE AND DATA SOURCE
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HotelTableViewCell
        if searchString != "" {
            cell.setCellWith(hotelsArray.filter{$0.hotelName.lowercased().contains(searchString.lowercased())}[indexPath.row])
        } else {
            cell.setCellWith(hotelsArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchString != "" {
        return hotelsArray.filter{$0.hotelName.lowercased().contains(searchString.lowercased())}.count
        }
        return hotelsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var selectedHotel = Hotel()
        if searchString != "" {
            selectedHotel = hotelsArray.filter{$0.hotelName.lowercased().contains(searchString.lowercased())}[indexPath.row]
        } else {
            selectedHotel = hotelsArray[indexPath.row]
        }
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "details") as! DetailsViewController
        detailsVC.selectedHotel = selectedHotel
        self.show(detailsVC, sender: self)
    }
}

//MARK: MAPKIT DELEGATE TO DRAW ALL HOTELS ON MAP
extension HomeViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            return nil
        }
        let reuseIdent = "annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdent)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdent)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage.init(named: "pin")
        }
        else {
            annotationView?.annotation = annotation
        }
        return annotationView!
    }
}

extension HomeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText
        self.hotelsTableView.reloadData()
    }
}

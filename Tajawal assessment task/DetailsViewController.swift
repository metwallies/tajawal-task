//
//  DetailsViewController.swift
//  Tajawal assessment task
//
//  Created by islam metwally on 3/9/18.
//  Copyright © 2018 Islam Metwally. All rights reserved.
//

import UIKit
import MapKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var hotelAddressLabel: UILabel!
    @IBOutlet weak var highRateLabel: UILabel!
    @IBOutlet weak var lowRateLabel: UILabel!
    //MARK: Variables
    var selectedHotel = Hotel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        hotelNameLabel.text = selectedHotel.hotelName
        hotelAddressLabel.text = selectedHotel.address
        highRateLabel.text = "AED \(selectedHotel.highRate)"
        lowRateLabel.text = "AED \(selectedHotel.lowRate)"
        
        if selectedHotel.lowRate < selectedHotel.highRate {
            let attributedString = NSMutableAttributedString(string: self.highRateLabel.text!, attributes: [NSAttributedStringKey.strikethroughStyle: 1])
            self.highRateLabel.attributedText = attributedString
        }
        
        let annotation = selectedHotel.setMapAnnotationPoint()
        self.mapView.addAnnotation(annotation)
        
        let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D.init(latitude: selectedHotel.lat, longitude: selectedHotel.lon), fromDistance: 1000.0, pitch: 0, heading: 0)
        self.mapView.setCamera(camera, animated: true)
    }
}

extension DetailsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            return nil
        }
        let reuseIdent = "annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdent)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdent)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage.init(named: "pin")
        }
        else {
            annotationView?.annotation = annotation
        }
        return annotationView!
    }
}

